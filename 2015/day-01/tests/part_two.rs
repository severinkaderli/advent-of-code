use aoc2015_01;
use utils::get_input;

#[test]
fn input() {
    assert_eq!(1795, aoc2015_01::part_two(&get_input("input.txt")));
}

#[test]
fn example_1() {
    assert_eq!(1, aoc2015_01::part_two(")"));
}

#[test]
fn example_2() {
    assert_eq!(5, aoc2015_01::part_two("()())"));
}
