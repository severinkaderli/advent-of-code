use aoc2015_01;
use utils::get_input;

#[test]
fn input() {
    assert_eq!(74, aoc2015_01::part_one(&get_input("input.txt")));
}

#[test]
fn example_1() {
    assert_eq!(0, aoc2015_01::part_one("(())"));
}

#[test]
fn example_2() {
    assert_eq!(0, aoc2015_01::part_one("()()"));
}

#[test]
fn example_3() {
    assert_eq!(3, aoc2015_01::part_one("((("));
}

#[test]
fn example_4() {
    assert_eq!(3, aoc2015_01::part_one("(()(()("));
}

#[test]
fn example_5() {
    assert_eq!(3, aoc2015_01::part_one("))((((("));
}

#[test]
fn example_6() {
    assert_eq!(-1, aoc2015_01::part_one("))("));
}

#[test]
fn example_7() {
    assert_eq!(-3, aoc2015_01::part_one(")))"));
}

#[test]
fn example_8() {
    assert_eq!(-3, aoc2015_01::part_one(")())())"));
}
