pub fn part_one(input: &str) -> i32 {
    let mut floor = 0;

    for c in input.chars() {
        match c {
            '(' => floor += 1,
            ')' => floor -= 1,
            _ => (),
        }
    }

    floor
}

pub fn part_two(input: &str) -> u32 {
    let mut floor = 0;

    for (index, c) in input.chars().enumerate() {
        match c {
            '(' => floor += 1,
            ')' => floor -= 1,
            _ => (),
        }

        if floor < 0 {
            return (index + 1) as u32;
        }
    }

    0
}
