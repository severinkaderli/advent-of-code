use aoc2015_02;
use utils::get_input;

#[test]
fn input() {
    assert_eq!(1598415, aoc2015_02::part_one(&get_input("input.txt")));
}

#[test]
fn example_1() {
    assert_eq!(58, aoc2015_02::part_one("2x3x4"));
}

#[test]
fn example_2() {
    assert_eq!(43, aoc2015_02::part_one("1x1x10"));
}

#[test]
fn example_3() {
    assert_eq!(101, aoc2015_02::part_one("2x3x4\n1x1x10"));
}
