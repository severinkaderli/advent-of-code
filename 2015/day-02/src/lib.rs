fn parse_line(line: &str) -> Vec<i32> {
    let mut values: Vec<i32> = line.split('x').map(|val| val.parse().unwrap()).collect();
    values.sort();
    values
}

pub fn part_one(input: &str) -> i32 {
    let mut sum = 0;

    for line in input.lines() {
        let values = parse_line(line);
        sum += 2 * values[0] * values[1]
            + 2 * values[0] * values[2]
            + 2 * values[1] * values[2]
            + values[0] * values[1];
    }

    sum
}

pub fn part_two(input: &str) -> i32 {
    let mut sum = 0;

    for line in input.lines() {
        let values = parse_line(line);
        sum += values[0] * 2 + values[1] * 2 + values[0] * values[1] * values[2];
    }

    sum
}
