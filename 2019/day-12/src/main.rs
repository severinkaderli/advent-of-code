use aoc2019_12;
use utils::{benchmark, get_input};

fn main() {
    let input = &get_input("input.txt");
    benchmark("2019 - Day 12 - Part 1", || {
        aoc2019_12::part_one(input, 1000);
    });

    benchmark("2019 - Day 12 - Part 2", || {
        aoc2019_12::part_two(input);
    });
}
