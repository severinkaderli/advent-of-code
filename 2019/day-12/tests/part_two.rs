use aoc2019_12;
use utils::get_input;

#[test]
fn input() {
    assert_eq!(
        318382803780324,
        aoc2019_12::part_two(&get_input("input.txt"))
    );
}

#[test]
fn example_1() {
    assert_eq!(2772, aoc2019_12::part_two(&get_input("example_1.txt")));
}

#[test]
fn example_2() {
    assert_eq!(
        4686774924,
        aoc2019_12::part_two(&get_input("example_2.txt"))
    );
}
