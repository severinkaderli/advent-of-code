use aoc2019_12;
use utils::get_input;

#[test]
fn input() {
    assert_eq!(9958, aoc2019_12::part_one(&get_input("input.txt"), 1000));
}

#[test]
fn example_1() {
    assert_eq!(179, aoc2019_12::part_one(&get_input("example_1.txt"), 10));
}

#[test]
fn example_2() {
    assert_eq!(1940, aoc2019_12::part_one(&get_input("example_2.txt"), 100));
}
