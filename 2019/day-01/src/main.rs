use aoc2019_01;
use utils::{benchmark, get_input};

fn main() {
    let input = &get_input("input.txt");
    benchmark("2019 - Day 01 - Part 1", || {
        aoc2019_01::part_one(input);
    });

    benchmark("2019 - Day 01 - Part 2", || {
        aoc2019_01::part_two(input);
    });
}
