fn calculate_fuel(mass: i32) -> i32 {
    let fuel = mass / 3 - 2;
    if fuel <= 0 {
        0
    } else {
        fuel + calculate_fuel(fuel)
    }
}

pub fn part_one(input: &str) -> i32 {
    input
        .lines()
        .map(|x| x.parse::<i32>().unwrap())
        .map(|mass| mass / 3 - 2)
        .sum()
}

pub fn part_two(input: &str) -> i32 {
    input
        .lines()
        .map(|x| x.parse::<i32>().unwrap())
        .map(calculate_fuel)
        .sum()
}
