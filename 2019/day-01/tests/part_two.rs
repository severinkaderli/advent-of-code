use aoc2019_01;
use utils::get_input;

#[test]
fn input() {
    assert_eq!(5244112, aoc2019_01::part_two(&get_input("input.txt")));
}

#[test]
fn example_1() {
    assert_eq!(2, aoc2019_01::part_two("14"));
}

#[test]
fn example_2() {
    assert_eq!(966, aoc2019_01::part_two("1969"));
}

#[test]
fn example_3() {
    assert_eq!(50346, aoc2019_01::part_two("100756"));
}
