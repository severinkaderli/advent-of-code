use aoc2019_01;
use utils::get_input;

#[test]
fn input() {
    assert_eq!(3497998, aoc2019_01::part_one(&get_input("input.txt")));
}

#[test]
fn example_1() {
    assert_eq!(2, aoc2019_01::part_one("12"));
}

#[test]
fn example_2() {
    assert_eq!(2, aoc2019_01::part_one("14"));
}

#[test]
fn example_3() {
    assert_eq!(4, aoc2019_01::part_one("12\n14"));
}

#[test]
fn example_4() {
    assert_eq!(654, aoc2019_01::part_one("1969"));
}

#[test]
fn example_5() {
    assert_eq!(33583, aoc2019_01::part_one("100756"));
}
