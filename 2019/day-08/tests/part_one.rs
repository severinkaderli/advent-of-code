use aoc2019_08;
use utils::get_input;

#[test]
fn input() {
    assert_eq!(1072, aoc2019_08::part_one(&get_input("input.txt"), 25, 6));
}

#[test]
fn example_1() {
    assert_eq!(1, aoc2019_08::part_one("123456789012", 3, 2));
}
