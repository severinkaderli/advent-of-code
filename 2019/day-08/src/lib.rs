fn get_digits(input: &str) -> Vec<i32> {
    input
        .trim()
        .chars()
        .map(|c| c.to_digit(10).unwrap() as i32)
        .collect()
}

fn output_image(image: &[&str], width: i32, height: i32) {
    for i in 0..(width * height) {
        print!("{}", image[i as usize]);

        if (i + 1) % width == 0 {
            print!("\n");
        }
    }
}

pub fn part_one(input: &str, width: i32, height: i32) -> i32 {
    let digits = get_digits(input);
    let mut layers: Vec<&[i32]> = digits.chunks((width * height) as usize).collect();
    layers.sort_by_key(|x| x.iter().filter(|x| **x == 0).count());
    let layer = layers[0];
    let ones = layer.iter().filter(|x| **x == 1).count();
    let twos = layer.iter().filter(|x| **x == 2).count();
    (ones * twos) as i32
}

pub fn part_two(input: &str, width: i32, height: i32) {
    let area = width * height;
    let digits = get_digits(input);
    let layers: Vec<&[i32]> = digits.chunks(area as usize).collect();
    let mut image = vec![" "; area as usize];
    for layer in layers.iter().rev() {
        for i in 0..layer.len() {
            let pixel = layer[i];
            match pixel {
                0 => image[i] = " ",
                1 => image[i] = "X",
                _ => (),
            }
        }
    }
    output_image(&image, width, height);
}
