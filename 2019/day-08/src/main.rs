use aoc2019_08;
use utils::{benchmark, get_input};

fn main() {
    let input = &get_input("input.txt");
    benchmark("2019 - Day 08 - Part 1", || {
        aoc2019_08::part_one(input, 25, 6);
    });

    benchmark("2019 - Day 08 - Part 2", || {
        aoc2019_08::part_two(input, 25, 6);
    });
}
