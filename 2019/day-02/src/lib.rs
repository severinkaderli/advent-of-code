fn parse_input(input: &str) -> Vec<i32> {
    input
        .trim()
        .split(',')
        .map(|x| x.parse().unwrap())
        .collect()
}

fn execute_intcode(mut program: Vec<i32>) -> i32 {
    let mut ip = 0;
    loop {
        let out_addr = program[ip + 3] as usize;
        let in_addr_1 = program[ip + 1] as usize;
        let in_addr_2 = program[ip + 2] as usize;

        match program[ip] {
            1 => program[out_addr] = program[in_addr_1] + program[in_addr_2],
            2 => program[out_addr] = program[in_addr_1] * program[in_addr_2],
            99 => break,
            _ => panic!("Unknown opcode: {}", program[ip]),
        }
        ip += 4
    }

    program[0]
}

pub fn part_one(input: &str) -> i32 {
    let mut program = parse_input(input);
    program[1] = 12;
    program[2] = 2;

    execute_intcode(program)
}

pub fn part_two(input: &str) -> i32 {
    let program = parse_input(input);

    for noun in 0..99 {
        for verb in 0..99 {
            let mut program_clone = program.clone();
            program_clone[1] = noun;
            program_clone[2] = verb;

            if execute_intcode(program_clone) == 19_690_720 {
                return 100 * noun + verb;
            }
        }
    }
    panic!("No solution found!");
}
