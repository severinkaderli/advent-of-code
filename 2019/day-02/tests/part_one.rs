use aoc2019_02;
use utils::get_input;

#[test]
fn input() {
    assert_eq!(5434663, aoc2019_02::part_one(&get_input("input.txt")));
}
