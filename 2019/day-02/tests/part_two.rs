use aoc2019_02;
use utils::get_input;

#[test]
fn input() {
    assert_eq!(4559, aoc2019_02::part_two(&get_input("input.txt")));
}
