use aoc2019_09;
use utils::get_input;

#[test]
fn input() {
    assert_eq!(66113, aoc2019_09::part_two(&get_input("input.txt")));
}
