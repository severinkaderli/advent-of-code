use aoc2019_09;
use utils::get_input;

#[test]
fn input() {
    assert_eq!(2941952859, aoc2019_09::part_one(&get_input("input.txt")));
}

#[test]
fn example_1() {
    assert_eq!(
        1125899906842624,
        aoc2019_09::part_one("104,1125899906842624,99")
    );
}

#[test]
fn example_2() {
    assert_eq!(
        109,
        aoc2019_09::part_one("109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99")
    );
}
