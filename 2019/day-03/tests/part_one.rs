use aoc2019_03;
use utils::get_input;

#[test]
fn input() {
    assert_eq!(225, aoc2019_03::part_one(&get_input("input.txt")));
}

#[test]
fn example_1() {
    assert_eq!(6, aoc2019_03::part_one("R8,U5,L5,D3\nU7,R6,D4,L4"));
}

#[test]
fn example_2() {
    assert_eq!(
        159,
        aoc2019_03::part_one("R75,D30,R83,U83,L12,D49,R71,U7,L72\nU62,R66,U55,R34,D71,R55,D58,R83")
    );
}

#[test]
fn example_3() {
    assert_eq!(
        135,
        aoc2019_03::part_one(
            "R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51\nU98,R91,D20,R16,D67,R40,U7,R15,U6,R7"
        )
    );
}
