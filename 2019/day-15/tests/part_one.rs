use aoc2019_15;
use utils::get_input;

#[test]
fn input() {
    assert_eq!(296, aoc2019_15::part_one(&get_input("input.txt")));
}
