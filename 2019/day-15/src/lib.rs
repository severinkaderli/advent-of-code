use pathfinding::prelude::dijkstra;
use std::collections::HashMap;
use utils::intcode::VM;
use utils::Direction;
use utils::Point;

#[derive(PartialEq, Eq)]
enum Tile {
    Air,
    Wall,
    Start,
    Unknown,
    OxygenStation,
}

fn get_value_from_direction(direction: Direction) -> i64 {
    match direction {
        Direction::Up => 1,
        Direction::Down => 2,
        Direction::Right => 3,
        Direction::Left => 4,
    }
}

fn get_map(input: &str) -> HashMap<Point, Tile> {
    let mut vm = VM::from(input);
    let mut map: HashMap<Point, Tile> = HashMap::new();
    let mut current_point = Point { x: 0, y: 0 };
    map.insert(current_point.clone(), Tile::Start);

    let mut current_direction = Direction::Left;
    let mut i = 0;
    loop {
        vm.add_input(get_value_from_direction(current_direction.clone()));
        vm.run();

        let status = vm.get_output();

        match status {
            0 => {
                let mut wall_point = current_point.clone();
                wall_point.add(&current_direction.get_point());

                map.entry(wall_point).or_insert(Tile::Wall);
                current_direction = current_direction.turn_left();
            }
            1 => {
                current_point.add(&current_direction.get_point());
                let copy = current_point.clone();
                map.entry(copy).or_insert(Tile::Air);
                current_direction = current_direction.turn_right();
            }
            2 => {
                current_point.add(&current_direction.get_point());
                let copy = current_point.clone();
                map.entry(copy).or_insert(Tile::OxygenStation);
                current_direction = current_direction.turn_right();
                i += 1;

                if i > 2 {
                    break;
                }
            }
            _ => panic!("Unknown status code!"),
        }
    }

    map
}

fn get_oxygen_station(map: &HashMap<Point, Tile>) -> Point {
    map.iter()
        .filter(|(_, v)| **v == Tile::OxygenStation)
        .nth(0)
        .unwrap()
        .0
        .clone()
}

fn successors(map: &HashMap<Point, Tile>, point: &Point) -> Vec<(Point, usize)> {
    map.iter()
        .filter(|(p, tile)| {
            if **tile == Tile::Wall || **tile == Tile::Unknown {
                return false;
            }

            (p.x == point.x + 1 && p.y == point.y)
                || (p.x == point.x - 1 && p.y == point.y)
                || (p.y == point.y + 1 && p.x == point.x)
                || (p.y == point.y - 1 && p.x == point.x)
        })
        .map(|p| (p.0.clone(), 1))
        .collect()
}

pub fn part_one(input: &str) -> usize {
    let map = get_map(input);
    let oxygen_station = get_oxygen_station(&map);
    let result = dijkstra(
        &Point { x: 0, y: 0 },
        |p| successors(&map, p),
        |p| *p == oxygen_station,
    )
    .unwrap();
    result.1
}

pub fn part_two(input: &str) -> usize {
    let map = get_map(input);
    let oxygen_station = get_oxygen_station(&map);

    // TODO: Find a better solution than this.
    map.iter()
        .filter(|(_, tile)| {
            if **tile == Tile::Wall || **tile == Tile::Unknown {
                return false;
            }

            true
        })
        .map(|(k, _)| {
            let result = dijkstra(&oxygen_station, |p| successors(&map, &p), |p| *p == *k).unwrap();
            result.1
        })
        .max()
        .unwrap()
}
