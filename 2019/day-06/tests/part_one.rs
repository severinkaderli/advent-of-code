use aoc2019_06;
use utils::get_input;

#[test]
fn input() {
    assert_eq!(145250, aoc2019_06::part_one(&get_input("input.txt")));
}

#[test]
fn example_1() {
    assert_eq!(
        42,
        aoc2019_06::part_one("COM)B\nB)C\nC)D\nD)E\nE)F\nB)G\nG)H\nD)I\nE)J\nJ)K\nK)L")
    );
}
