use aoc2019_04;
use utils::get_input;

#[test]
fn input() {
    assert_eq!(1135, aoc2019_04::part_two(&get_input("input.txt")));
}

#[test]
fn example_1() {
    assert_eq!(1, aoc2019_04::part_two("112233-112233"));
}

#[test]
fn example_2() {
    assert_eq!(0, aoc2019_04::part_two("123444-123444"));
}

#[test]
fn example_3() {
    assert_eq!(1, aoc2019_04::part_two("111122-111122"));
}
