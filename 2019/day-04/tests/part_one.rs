use aoc2019_04;
use utils::get_input;

#[test]
fn input() {
    assert_eq!(1660, aoc2019_04::part_one(&get_input("input.txt")));
}

#[test]
fn example_1() {
    assert_eq!(1, aoc2019_04::part_one("111111-111111"));
}

#[test]
fn example_2() {
    assert_eq!(0, aoc2019_04::part_one("223450-223450"));
}

#[test]
fn example_3() {
    assert_eq!(0, aoc2019_04::part_one("123789-123789"));
}
