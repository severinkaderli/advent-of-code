use aoc2019_14;
use utils::get_input;

#[test]
fn input() {
    assert_eq!(319014, aoc2019_14::part_one(&get_input("input.txt")));
}

#[test]
fn example_1() {
    assert_eq!(13312, aoc2019_14::part_one(&get_input("example_1.txt")));
}

#[test]
fn example_2() {
    assert_eq!(180697, aoc2019_14::part_one(&get_input("example_2.txt")));
}

#[test]
fn example_3() {
    assert_eq!(2210736, aoc2019_14::part_one(&get_input("example_3.txt")));
}
