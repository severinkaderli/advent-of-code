use aoc2019_14;
use utils::get_input;

#[test]
fn input() {
    assert_eq!(4076490, aoc2019_14::part_two(&get_input("input.txt")));
}

#[test]
fn example_1() {
    assert_eq!(82892753, aoc2019_14::part_two(&get_input("example_1.txt")));
}

#[test]
fn example_2() {
    assert_eq!(5586022, aoc2019_14::part_two(&get_input("example_2.txt")));
}

#[test]
fn example_3() {
    assert_eq!(460664, aoc2019_14::part_two(&get_input("example_3.txt")));
}
