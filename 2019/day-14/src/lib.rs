use std::collections::HashMap;

#[derive(Debug, PartialEq, Eq, Hash)]
struct Chemical {
    name: String,
    amount: i64,
}

fn get_reactions(input: &str) -> HashMap<Chemical, Vec<Chemical>> {
    let mut reactions = HashMap::new();

    for line in input.trim().lines() {
        let parts: Vec<&str> = line.split("=>").collect();
        let ingredients: Vec<&str> = parts[0].trim().split(',').collect();
        let mut reaction_ingredients = Vec::new();
        for ingredient in ingredients.iter() {
            let ingredient_parts: Vec<&str> = ingredient.trim().split(' ').collect();
            reaction_ingredients.push(Chemical {
                name: ingredient_parts[1].to_owned(),
                amount: ingredient_parts[0].trim().parse().unwrap(),
            });
        }

        let result_parts: Vec<&str> = parts[1].trim().split(' ').collect();
        let result = Chemical {
            name: result_parts[1].to_owned(),
            amount: result_parts[0].trim().parse().unwrap(),
        };
        reactions.insert(result, reaction_ingredients);
    }

    reactions
}

fn get_required_ore(
    reactions: &HashMap<Chemical, Vec<Chemical>>,
    chemical: &str,
    amount: i64,
    surplus_map: &mut HashMap<String, i64>,
) -> i64 {
    if chemical == "ORE" {
        return amount;
    }

    let mut amount_needed = amount;
    let surplus_entry = surplus_map.entry(chemical.to_owned()).or_default();

    if *surplus_entry >= amount_needed {
        *surplus_entry -= amount_needed;
        return 0;
    } else {
        amount_needed -= *surplus_entry;
        *surplus_entry = 0;
    }

    let reaction_value = reactions
        .iter()
        .filter(|(k, _)| k.name == chemical)
        .nth(0)
        .unwrap();
    let reaction_quantity = reaction_value.0.amount;
    let ingredients = reaction_value.1;

    let number_of_reactions = (amount_needed as f64 / reaction_quantity as f64).ceil() as i64;
    let surplus = (reaction_quantity * number_of_reactions) - amount_needed;
    *surplus_map.entry(chemical.to_owned()).or_default() += surplus;

    let mut required_ore = 0;
    for ingredient in ingredients {
        required_ore += get_required_ore(
            &reactions,
            &ingredient.name,
            ingredient.amount * number_of_reactions,
            surplus_map,
        );
    }
    required_ore
}

pub fn part_one(input: &str) -> i64 {
    let reactions = get_reactions(input);
    get_required_ore(&reactions, "FUEL", 1, &mut HashMap::new())
}

// TODO: Find a correct solution for part 2
pub fn part_two(input: &str) -> i64 {
    let reactions = get_reactions(input);
    let max_ores = 1_000_000_000_000i64;
    let min_ores_per_fuel = get_required_ore(&reactions, "FUEL", 1, &mut HashMap::new());
    let mut low = max_ores / min_ores_per_fuel;
    let mut high = low * 2;
    let mut mid = 0;
    while low <= high {
        mid = (low + high) / 2;
        let ores = get_required_ore(&reactions, "FUEL", mid, &mut HashMap::new());

        if ores >= max_ores {
            high = mid - 1;
        } else {
            low = mid + 1;
        }
    }

    mid
}
