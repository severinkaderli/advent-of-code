use aoc2019_16;
use utils::{benchmark, get_input};

fn main() {
    let input = &get_input("input.txt");
    benchmark("2019 - Day 16 - Part 1", || {
        aoc2019_16::part_one(input, 100);
    });

    benchmark("2019 - Day 16 - Part 2", || {
        aoc2019_16::part_two(input);
    });
}
