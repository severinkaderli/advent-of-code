fn get_digits(input: &str) -> Vec<i32> {
    input
        .trim()
        .chars()
        .map(|c| c.to_digit(10).unwrap() as i32)
        .collect()
}

fn make_pattern(pattern: &Vec<i32>, index: usize) -> Vec<i32> {
    let mut new_pattern = Vec::new();

    for val in pattern {
        for i in 0..=index {
            new_pattern.push(*val);
        }
    }
    new_pattern
}

fn phase(digits: Vec<i32>) -> Vec<i32> {
    let pattern = vec![0, 1, 0, -1];
    let mut new_digits = Vec::new();
    for output_index in 0..digits.len() {
        let mut output_value = 0;
        let new_pattern = make_pattern(&pattern, output_index);
        for (i, digit) in digits.iter().enumerate() {
            output_value += *digit * new_pattern[(i + 1) % new_pattern.len()];
        }
        new_digits.insert(output_index, (output_value % 10).abs());
    }
    new_digits
}

pub fn part_one(input: &str, phases: i32) -> i32 {
    let mut digits = get_digits(input);

    for i in 0..phases {
        println!("Phase {}", i + 1);
        digits = phase(digits);
    }

    println!("{:?}", digits);
    digits[..8]
        .iter()
        .map(|x| x.to_string())
        .collect::<String>()
        .parse()
        .unwrap()
}

pub fn part_two(input: &str) -> i32 {
    0
}
