use aoc2019_16;
use utils::get_input;

#[test]
fn input() {
    assert_eq!(302, aoc2019_16::part_two(&get_input("input.txt")));
}
