use aoc2019_16;
use utils::get_input;

#[test]
fn input() {
    assert_eq!(42945143, aoc2019_16::part_one(&get_input("input.txt"), 100));
}

#[test]
fn example_1() {
    assert_eq!(48226158, aoc2019_16::part_one("12345678", 1));
}
#[test]
fn example_2() {
    assert_eq!(34040438, aoc2019_16::part_one("12345678", 2));
}

#[test]
fn example_3() {
    assert_eq!(03415518, aoc2019_16::part_one("12345678", 3));
}

#[test]
fn example_4() {
    assert_eq!(01029498, aoc2019_16::part_one("12345678", 4));
}

#[test]
fn example_5() {
    assert_eq!(
        24176176,
        aoc2019_16::part_one("80871224585914546619083218645595", 100)
    );
}

#[test]
fn example_6() {
    assert_eq!(
        73745418,
        aoc2019_16::part_one("19617804207202209144916044189917", 100)
    );
}

#[test]
fn example_7() {
    assert_eq!(
        52432133,
        aoc2019_16::part_one("69317163492948606335995924319873", 100)
    );
}
