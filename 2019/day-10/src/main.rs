use aoc2019_10;
use utils::{benchmark, get_input, Point};

fn main() {
    let input = &get_input("input.txt");
    benchmark("2019 - Day 10 - Part 1", || {
        aoc2019_10::part_one(input);
    });

    benchmark("2019 - Day 10 - Part 2", || {
        aoc2019_10::part_two(input, Point { x: 13, y: 17 });
    });
}
