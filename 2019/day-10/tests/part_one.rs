use aoc2019_10;
use utils::get_input;

#[test]
fn input() {
    assert_eq!(269, aoc2019_10::part_one(&get_input("input.txt")));
}

#[test]
fn example_1() {
    assert_eq!(8, aoc2019_10::part_one(".#..#\n.....\n#####\n....#\n...##"));
}
