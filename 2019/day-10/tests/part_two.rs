use aoc2019_10;
use utils::{get_input, Point};

#[test]
fn input() {
    assert_eq!(
        612,
        aoc2019_10::part_two(&get_input("input.txt"), Point { x: 13, y: 17 })
    );
}

#[test]
fn example_1() {
    assert_eq!(
        802,
        aoc2019_10::part_two(&get_input("large_example.txt"), Point { x: 11, y: 13 })
    );
}
