use aoc2019_11;
use utils::get_input;

#[test]
fn input() {
    assert_eq!(2276, aoc2019_11::part_one(&get_input("input.txt")));
}
