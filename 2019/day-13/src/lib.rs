use std::cmp::Ordering;
use std::collections::HashMap;
use utils::intcode::{ExitCode, VM};
use utils::Point;

#[derive(PartialEq, Eq)]
enum Tile {
    Empty,
    Wall,
    Block,
    Paddle,
    Ball,
}

impl Tile {
    pub fn from(value: i64) -> Self {
        match value {
            0 => Tile::Empty,
            1 => Tile::Wall,
            2 => Tile::Block,
            3 => Tile::Paddle,
            4 => Tile::Ball,
            _ => panic!("Invalid tile {}", value),
        }
    }

    pub fn draw(&self) {
        let output = match &self {
            Tile::Empty => ' ',
            Tile::Wall => '|',
            Tile::Block => '#',
            Tile::Paddle => '_',
            Tile::Ball => '0',
        };

        print!("{}", output);
    }
}

struct BoardState {
    pub ball_position: Point,
    pub paddle_position: Point,
    pub number_of_blocks: i32,
}

impl BoardState {
    pub fn new() -> BoardState {
        BoardState {
            ball_position: Point { x: -1, y: -1 },
            paddle_position: Point { x: -1, y: -1 },
            number_of_blocks: 0,
        }
    }
}

fn draw_board(field: &mut HashMap<Point, Tile>) {
    let max_x = field.keys().max_by_key(|x| x.x).unwrap().x;
    let max_y = field.keys().max_by_key(|x| x.y).unwrap().y;
    for y in 0..=max_y {
        for x in 0..=max_x {
            let tile = field.entry(Point { x, y }).or_insert(Tile::Empty);
            tile.draw();
        }

        println!();
    }
}

fn update_field(vm: &mut VM, field: &mut HashMap<Point, Tile>, state: &mut BoardState) {
    for tile in vm.get_output_iter().collect::<Vec<i64>>().chunks(3) {
        if tile[0] == -1 {
            println!("Score: {}", tile[2]);
            continue;
        }

        let tile_type = Tile::from(tile[2]);
        let point = Point {
            x: tile[0] as i32,
            y: tile[1] as i32,
        };

        match tile_type {
            Tile::Ball => state.ball_position = point.clone(),
            Tile::Paddle => state.paddle_position = point.clone(),
            Tile::Block => state.number_of_blocks += 1,
            _ => (),
        };

        field.insert(point, tile_type);
    }
}

pub fn part_one(input: &str) -> i32 {
    let mut vm = VM::from(input);
    vm.run();
    let mut field = HashMap::new();
    let mut state = BoardState::new();
    update_field(&mut vm, &mut field, &mut state);
    state.number_of_blocks
}

pub fn part_two(input: &str) {
    let mut vm = VM::from(input);
    let mut state = BoardState::new();
    vm.set_memory(0, 2);
    let mut field = HashMap::new();
    loop {
        let exit_code = vm.run();

        update_field(&mut vm, &mut field, &mut state);
        draw_board(&mut field);

        if exit_code == ExitCode::Stop {
            break;
        }

        if exit_code == ExitCode::NeedInput {
            let joystick = match state.paddle_position.x.cmp(&state.ball_position.x) {
                Ordering::Less => 1,
                Ordering::Equal => 0,
                Ordering::Greater => -1,
            };

            vm.add_input(joystick);
        }
    }
}
