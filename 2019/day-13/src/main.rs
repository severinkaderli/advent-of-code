use aoc2019_13;
use utils::{benchmark, get_input};

fn main() {
    let input = &get_input("input.txt");
    benchmark("2019 - Day 13 - Part 1", || {
        aoc2019_13::part_one(input);
    });

    benchmark("2019 - Day 13 - Part 2", || {
        aoc2019_13::part_two(input);
    });
}
