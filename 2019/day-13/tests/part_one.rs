use aoc2019_13;
use utils::get_input;

#[test]
fn input() {
    assert_eq!(333, aoc2019_13::part_one(&get_input("input.txt")));
}
