use aoc2019_05;
use utils::{benchmark, get_input};

fn main() {
    let input = &get_input("input.txt");
    benchmark("2019 - Day 05 - Part 1", || {
        aoc2019_05::part_one(input);
    });

    benchmark("2019 - Day 05 - Part 2", || {
        aoc2019_05::part_two(input);
    });
}
