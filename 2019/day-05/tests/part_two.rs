use aoc2019_05;
use utils::get_input;

#[test]
fn input() {
    assert_eq!(7161591, aoc2019_05::part_two(&get_input("input.txt")));
}
