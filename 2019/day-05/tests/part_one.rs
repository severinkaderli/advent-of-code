use aoc2019_05;
use utils::get_input;

#[test]
fn input() {
    assert_eq!(5577461, aoc2019_05::part_one(&get_input("input.txt")));
}
