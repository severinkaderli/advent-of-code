use std::collections::VecDeque;

pub struct VM {
    memory: Vec<i64>,
    ip: usize,
    input: VecDeque<i64>,
    output: VecDeque<i64>,
    exit_code: ExitCode,
    relative_base: i64,
}

#[derive(Clone, PartialEq, Debug)]
pub enum ExitCode {
    Running,
    NeedInput,
    Stop,
}

impl From<&str> for VM {
    fn from(input: &str) -> Self {
        let mut memory: Vec<i64> = input
            .trim()
            .split(',')
            .map(|x| x.trim().parse().unwrap())
            .collect();
        memory.resize(4096, 0);

        VM {
            memory,
            ip: 0,
            input: VecDeque::new(),
            output: VecDeque::new(),
            exit_code: ExitCode::Running,
            relative_base: 0,
        }
    }
}

impl VM {
    pub fn run(&mut self) -> ExitCode {
        self.set_exit_code(ExitCode::Running);
        loop {
            if !self.run_cycle() {
                return self.get_exit_code();
            }
        }
    }

    fn get_exit_code(&self) -> ExitCode {
        self.exit_code.clone()
    }

    fn set_exit_code(&mut self, exit_code: ExitCode) {
        self.exit_code = exit_code;
    }

    pub fn set_memory(&mut self, address: usize, value: i64) {
        self.memory[address] = value;
    }

    pub fn add_input(&mut self, input: i64) {
        self.input.push_back(input);
    }

    pub fn get_output(&mut self) -> i64 {
        self.output.pop_front().unwrap()
    }

    pub fn get_output_iter(&mut self) -> std::collections::vec_deque::Drain<'_, i64> {
        self.output.drain(0..self.output.len())
    }

    fn get_mode(&self, param: usize) -> i64 {
        let opcode = self.memory[self.ip];
        let power = 10i64.pow((param + 1) as u32);
        (opcode / power) % 10
    }

    fn get_param(&self, param: usize) -> i64 {
        let opcode = self.memory[self.ip];
        let power = 10i64.pow((param + 1) as u32);
        let parameter_mode = (opcode / power) % 10;

        match self.get_mode(param) {
            0 => self.memory[self.memory[self.ip + param] as usize],
            1 => self.memory[self.ip + param],
            2 => {
                let addr = self.memory[self.ip + param] + self.relative_base;
                self.memory[addr as usize]
            }
            _ => panic!("Unknown parameter mode: {}", parameter_mode),
        }
    }

    fn get_address(&self, position: usize) -> usize {
        match self.get_mode(position) {
            2 => (self.memory[self.ip + position] + self.relative_base) as usize,
            _ => self.memory[self.ip + position] as usize,
        }
    }

    fn op_add(&mut self) {
        let addr = self.get_address(3);
        self.memory[addr] = self.get_param(1) + self.get_param(2);
        self.ip += 4
    }

    fn op_mul(&mut self) {
        let addr = self.get_address(3);
        self.memory[addr] = self.get_param(1) * self.get_param(2);
        self.ip += 4
    }

    fn op_in(&mut self) {
        if self.input.is_empty() {
            self.exit_code = ExitCode::NeedInput;
            return;
        }

        let addr = self.get_address(1);
        self.memory[addr] = self.input.pop_front().unwrap();
        self.ip += 2;
    }

    fn op_out(&mut self) {
        self.output.push_back(self.get_param(1));
        self.ip += 2
    }

    fn op_jnz(&mut self) {
        if self.get_param(1) != 0 {
            self.ip = self.get_param(2) as usize;
        } else {
            self.ip += 3
        }
    }

    fn op_jz(&mut self) {
        if self.get_param(1) == 0 {
            self.ip = self.get_param(2) as usize;
        } else {
            self.ip += 3
        }
    }

    fn op_lt(&mut self) {
        let addr = self.get_address(3);
        if self.get_param(1) < self.get_param(2) {
            self.memory[addr] = 1;
        } else {
            self.memory[addr] = 0;
        }
        self.ip += 4
    }

    fn op_eq(&mut self) {
        let addr = self.get_address(3);
        if self.get_param(1) == self.get_param(2) {
            self.memory[addr] = 1;
        } else {
            self.memory[addr] = 0;
        }
        self.ip += 4
    }

    fn op_rel(&mut self) {
        self.relative_base += self.get_param(1);
        self.ip += 2
    }

    fn run_cycle(&mut self) -> bool {
        let opcode = self.memory[self.ip] % 100;
        match opcode {
            1 => self.op_add(),
            2 => self.op_mul(),
            3 => self.op_in(),
            4 => self.op_out(),
            5 => self.op_jnz(),
            6 => self.op_jz(),
            7 => self.op_lt(),
            8 => self.op_eq(),
            9 => self.op_rel(),
            99 => self.set_exit_code(ExitCode::Stop),
            _ => panic!("Unknown opcode: {}", self.memory[self.ip]),
        }

        if self.exit_code != ExitCode::Running {
            return false;
        }

        true
    }
}
