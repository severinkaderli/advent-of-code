use permutohedron::LexicalPermutation;
use std::cmp::Ordering;
use std::fs;
use std::time::Duration;
use std::time::Instant;

pub fn get_input(file: &str) -> String {
    fs::read_to_string(String::from("input/") + file).unwrap()
}

pub fn benchmark<F>(name: &str, function: F)
where
    F: Fn() -> (),
{
    let runs = 10;
    let mut total_duration = Duration::new(0, 0);
    for _i in 0..runs {
        let start = Instant::now();
        function();
        total_duration = total_duration.checked_add(start.elapsed()).unwrap();
    }

    println!(
        "{} time: {:?}",
        name,
        total_duration.checked_div(runs).unwrap()
    );
}

#[derive(PartialEq, PartialOrd, Eq, Hash, Clone, Debug)]
pub struct Point {
    pub x: i32,
    pub y: i32,
}
pub mod intcode;

impl Point {
    pub fn manhatten_distance(&self) -> i32 {
        self.x.abs() + self.y.abs()
    }

    pub fn add(&mut self, other: &Point) {
        self.x += other.x;
        self.y += other.y;
    }
}

impl Ord for Point {
    fn cmp(&self, other: &Self) -> Ordering {
        self.manhatten_distance().cmp(&other.manhatten_distance())
    }
}

pub fn get_permutations(input: Vec<i32>) -> Vec<Vec<i32>> {
    let mut state = input;
    let mut permutations = Vec::new();

    loop {
        permutations.push(state.clone());
        if !state.next_permutation() {
            break;
        }
    }

    permutations
}

#[derive(PartialEq, PartialOrd, Eq, Hash, Clone, Debug)]
pub enum Direction {
    Up,
    Down,
    Left,
    Right,
}

impl Direction {
    pub fn turn_left(&self) -> Direction {
        match &self {
            Direction::Up => Direction::Left,
            Direction::Down => Direction::Right,
            Direction::Left => Direction::Down,
            Direction::Right => Direction::Up,
        }
    }

    pub fn turn_right(&self) -> Direction {
        match &self {
            Direction::Up => Direction::Right,
            Direction::Down => Direction::Left,
            Direction::Left => Direction::Up,
            Direction::Right => Direction::Down,
        }
    }

    pub fn get_point(&self) -> Point {
        match &self {
            Direction::Up => Point { x: 0, y: 1 },
            Direction::Down => Point { x: 0, y: -1 },
            Direction::Left => Point { x: -1, y: 0 },
            Direction::Right => Point { x: 1, y: 0 },
        }
    }
}
